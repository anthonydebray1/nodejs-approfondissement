const Article = require('./articles.schema');

async function createArticle(data) 
{
    return Article.create(data);
}

async function updateArticle(id, data, userId) 
{
    const article = await Article.findById(id);
    if (article.author.toString() !== userId.toString()) 
    {
        throw new Error('Unauthorized');
    }
    return Article.findByIdAndUpdate(id, data, { new: true });
}

async function findArticlesByUserId(userId) 
{
    return Article.find({ author: userId }).populate('author', '-password');
}

async function deleteArticle(id, userId) 
{
    const article = await Article.findById(id);
    if (article.author.toString() !== userId.toString()) 
    {
        throw new Error('Unauthorized');
    }
    return Article.findByIdAndDelete(id);
}

module.exports = { createArticle, updateArticle,findArticlesByUserId, deleteArticle };