const ArticleService = require('./articles.service');

async function create(req, res) 
{
  try 
  {
    const article = await ArticleService.createArticle({ ...req.body, author: req.user._id });
    res.status(201).json(article);
  } 
  catch (error) 
  {
    res.status(500).json({ message: error.message });
  }
}

async function update(req, res) 
{
  if (req.user.role !== 'admin') 
  {
    return res.status(403).json({ message: 'Forbidden' });
  }

  try 
  {
    const article = await ArticleService.updateArticle(req.params.id, req.body, req.user._id);
    res.json(article);
  } 
  catch (error) 
  {
    res.status(500).json({ message: error.message });
  }
}

async function remove(req, res) 
{
  if (req.user.role !== 'admin') 
  {
    return res.status(403).json({ message: 'Forbidden' });
  }

  try 
  {
    await ArticleService.deleteArticle(req.params.id, req.user._id);
    res.status(204).end();
  } 
  catch (error) 
  {
    res.status(500).json({ message: error.message });
  }
}

module.exports = { create, update, remove };