const request = require("supertest");
const { app } = require("../server"); // Assurez-vous que le chemin est correct
const jwt = require("jsonwebtoken");
const config = require("../config");
const mongoose = require("mongoose");
const mockingoose = require("mockingoose");
const Article = require('../api/articles/articles.schema');
const articlesService = require("../api/articles/articles.service");

describe("API articles tests", () => {
  let token;
  const USER_ID = "fakeUserId";
  const ARTICLE_ID = "fakeArticleId";
  const MOCK_ARTICLES = [
    {
      _id: ARTICLE_ID,
      title: "Test Article",
      content: "This is a test article content.",
      author: USER_ID,
    },
  ];
  const MOCK_ARTICLE_CREATED = {
    title: "New Article",
    content: "Content of the new article.",
    author: USER_ID,
  };

  beforeEach(() => {
    token = jwt.sign({ userId: USER_ID }, config.secretJwtToken);
    mockingoose(Article).toReturn(MOCK_ARTICLES, "find");
    mockingoose(Article).toReturn(MOCK_ARTICLE_CREATED, "save");
  });

  test("[Articles] Get All", async () => {
    const res = await request(app)
      .get(`/api/users/${USER_ID}/articles`)
      .set("x-access-token", token);
    expect(res.status).toBe(200);
    expect(res.body.length).toBeGreaterThan(0);
    expect(res.body[0].title).toBe(MOCK_ARTICLES[0].title);
  });

  test("[Articles] Create Article", async () => {
    const res = await request(app)
      .post("/api/articles")
      .send(MOCK_ARTICLE_CREATED)
      .set("x-access-token", token);
    expect(res.status).toBe(201);
    expect(res.body.title).toBe(MOCK_ARTICLE_CREATED.title);
  });

  test("Check articlesService.createArticle is called", async () => {
    const spy = jest
      .spyOn(articlesService, "createArticle")
      .mockImplementation(() => Promise.resolve(MOCK_ARTICLE_CREATED));
    await request(app)
      .post("/api/articles")
      .send(MOCK_ARTICLE_CREATED)
      .set("x-access-token", token);
    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockRestore();
  });

  afterEach(() => {
    mockingoose.resetAll();
    jest.restoreAllMocks();
  });
});